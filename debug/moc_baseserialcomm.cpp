/****************************************************************************
** Meta object code from reading C++ file 'baseserialcomm.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../inc/baseserialcomm.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'baseserialcomm.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_BaseSerialComm_t {
    QByteArrayData data[33];
    char stringdata0[320];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_BaseSerialComm_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_BaseSerialComm_t qt_meta_stringdata_BaseSerialComm = {
    {
QT_MOC_LITERAL(0, 0, 14), // "BaseSerialComm"
QT_MOC_LITERAL(1, 15, 16), // "slot_writtenData"
QT_MOC_LITERAL(2, 32, 0), // ""
QT_MOC_LITERAL(3, 33, 5), // "txBuf"
QT_MOC_LITERAL(4, 39, 8), // "BaudRate"
QT_MOC_LITERAL(5, 48, 8), // "Baud1200"
QT_MOC_LITERAL(6, 57, 8), // "Baud2400"
QT_MOC_LITERAL(7, 66, 8), // "Baud4800"
QT_MOC_LITERAL(8, 75, 8), // "Baud9600"
QT_MOC_LITERAL(9, 84, 9), // "Baud19200"
QT_MOC_LITERAL(10, 94, 9), // "Baud38400"
QT_MOC_LITERAL(11, 104, 9), // "Baud57600"
QT_MOC_LITERAL(12, 114, 10), // "Baud115200"
QT_MOC_LITERAL(13, 125, 10), // "Baud128000"
QT_MOC_LITERAL(14, 136, 10), // "Baud256000"
QT_MOC_LITERAL(15, 147, 11), // "Baud1000000"
QT_MOC_LITERAL(16, 159, 11), // "UnknownBaud"
QT_MOC_LITERAL(17, 171, 10), // "Terminator"
QT_MOC_LITERAL(18, 182, 4), // "NONE"
QT_MOC_LITERAL(19, 187, 11), // "CRLF_0x0D0A"
QT_MOC_LITERAL(20, 199, 7), // "CR_0x0D"
QT_MOC_LITERAL(21, 207, 7), // "LF_0xOA"
QT_MOC_LITERAL(22, 215, 17), // "UnknownTerminator"
QT_MOC_LITERAL(23, 233, 11), // "VerifyStyle"
QT_MOC_LITERAL(24, 245, 13), // "AddVerifyItem"
QT_MOC_LITERAL(25, 259, 4), // "ADD8"
QT_MOC_LITERAL(26, 264, 5), // "NADD8"
QT_MOC_LITERAL(27, 270, 4), // "XOR8"
QT_MOC_LITERAL(28, 275, 10), // "CRC_Modbus"
QT_MOC_LITERAL(29, 286, 10), // "CRC_Xmodem"
QT_MOC_LITERAL(30, 297, 5), // "CRC32"
QT_MOC_LITERAL(31, 303, 3), // "LRC"
QT_MOC_LITERAL(32, 307, 12) // "UnknownStyle"

    },
    "BaseSerialComm\0slot_writtenData\0\0txBuf\0"
    "BaudRate\0Baud1200\0Baud2400\0Baud4800\0"
    "Baud9600\0Baud19200\0Baud38400\0Baud57600\0"
    "Baud115200\0Baud128000\0Baud256000\0"
    "Baud1000000\0UnknownBaud\0Terminator\0"
    "NONE\0CRLF_0x0D0A\0CR_0x0D\0LF_0xOA\0"
    "UnknownTerminator\0VerifyStyle\0"
    "AddVerifyItem\0ADD8\0NADD8\0XOR8\0CRC_Modbus\0"
    "CRC_Xmodem\0CRC32\0LRC\0UnknownStyle"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_BaseSerialComm[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       3,   22, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   19,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, QMetaType::QByteArray,    3,

 // enums: name, flags, count, data
       4, 0x0,   12,   34,
      17, 0x0,    5,   58,
      23, 0x0,    9,   68,

 // enum data: key, value
       5, uint(BaseSerialComm::Baud1200),
       6, uint(BaseSerialComm::Baud2400),
       7, uint(BaseSerialComm::Baud4800),
       8, uint(BaseSerialComm::Baud9600),
       9, uint(BaseSerialComm::Baud19200),
      10, uint(BaseSerialComm::Baud38400),
      11, uint(BaseSerialComm::Baud57600),
      12, uint(BaseSerialComm::Baud115200),
      13, uint(BaseSerialComm::Baud128000),
      14, uint(BaseSerialComm::Baud256000),
      15, uint(BaseSerialComm::Baud1000000),
      16, uint(BaseSerialComm::UnknownBaud),
      18, uint(BaseSerialComm::NONE),
      19, uint(BaseSerialComm::CRLF_0x0D0A),
      20, uint(BaseSerialComm::CR_0x0D),
      21, uint(BaseSerialComm::LF_0xOA),
      22, uint(BaseSerialComm::UnknownTerminator),
      24, uint(BaseSerialComm::AddVerifyItem),
      25, uint(BaseSerialComm::ADD8),
      26, uint(BaseSerialComm::NADD8),
      27, uint(BaseSerialComm::XOR8),
      28, uint(BaseSerialComm::CRC_Modbus),
      29, uint(BaseSerialComm::CRC_Xmodem),
      30, uint(BaseSerialComm::CRC32),
      31, uint(BaseSerialComm::LRC),
      32, uint(BaseSerialComm::UnknownStyle),

       0        // eod
};

void BaseSerialComm::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        BaseSerialComm *_t = static_cast<BaseSerialComm *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->slot_writtenData((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject BaseSerialComm::staticMetaObject = {
    { &QSerialPort::staticMetaObject, qt_meta_stringdata_BaseSerialComm.data,
      qt_meta_data_BaseSerialComm,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *BaseSerialComm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *BaseSerialComm::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_BaseSerialComm.stringdata0))
        return static_cast<void*>(const_cast< BaseSerialComm*>(this));
    if (!strcmp(_clname, "QSerialPortInfo"))
        return static_cast< QSerialPortInfo*>(const_cast< BaseSerialComm*>(this));
    return QSerialPort::qt_metacast(_clname);
}

int BaseSerialComm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QSerialPort::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
