/****************************************************************************
** Meta object code from reading C++ file 'modbusmaster.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../inc/modbusmaster.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'modbusmaster.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_ModbusMaster_t {
    QByteArrayData data[51];
    char stringdata0[1079];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ModbusMaster_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ModbusMaster_t qt_meta_stringdata_ModbusMaster = {
    {
QT_MOC_LITERAL(0, 0, 12), // "ModbusMaster"
QT_MOC_LITERAL(1, 13, 18), // "signal_writtenData"
QT_MOC_LITERAL(2, 32, 0), // ""
QT_MOC_LITERAL(3, 33, 5), // "txBuf"
QT_MOC_LITERAL(4, 39, 21), // "signal_cmd01HProtocal"
QT_MOC_LITERAL(5, 61, 21), // "signal_cmd03HProtocal"
QT_MOC_LITERAL(6, 83, 21), // "signal_cmd05HProtocal"
QT_MOC_LITERAL(7, 105, 21), // "signal_cmd06HProtocal"
QT_MOC_LITERAL(8, 127, 21), // "signal_cmd10HProtocal"
QT_MOC_LITERAL(9, 149, 16), // "slots_RxCallback"
QT_MOC_LITERAL(10, 166, 18), // "slots_errorHandler"
QT_MOC_LITERAL(11, 185, 28), // "QSerialPort::SerialPortError"
QT_MOC_LITERAL(12, 214, 5), // "error"
QT_MOC_LITERAL(13, 220, 16), // "slots_waitForCRC"
QT_MOC_LITERAL(14, 237, 20), // "slots_cmd01HProtocal"
QT_MOC_LITERAL(15, 258, 20), // "slots_cmd03HProtocal"
QT_MOC_LITERAL(16, 279, 20), // "slots_cmd05HProtocal"
QT_MOC_LITERAL(17, 300, 20), // "slots_cmd06HProtocal"
QT_MOC_LITERAL(18, 321, 20), // "slots_cmd10HProtocal"
QT_MOC_LITERAL(19, 342, 27), // "slots_showBtnRightClickMenu"
QT_MOC_LITERAL(20, 370, 3), // "pos"
QT_MOC_LITERAL(21, 374, 17), // "slots_clicktoSend"
QT_MOC_LITERAL(22, 392, 16), // "slots_singleShot"
QT_MOC_LITERAL(23, 409, 21), // "on_btnRefresh_clicked"
QT_MOC_LITERAL(24, 431, 22), // "on_btnOpenPort_clicked"
QT_MOC_LITERAL(25, 454, 7), // "checked"
QT_MOC_LITERAL(26, 462, 29), // "on_txt03SlaveAddr_textChanged"
QT_MOC_LITERAL(27, 492, 4), // "arg1"
QT_MOC_LITERAL(28, 497, 27), // "on_txt03RegAddr_textChanged"
QT_MOC_LITERAL(29, 525, 26), // "on_txt03RegNum_textChanged"
QT_MOC_LITERAL(30, 552, 29), // "on_txt04SlaveAddr_textChanged"
QT_MOC_LITERAL(31, 582, 27), // "on_txt04RegAddr_textChanged"
QT_MOC_LITERAL(32, 610, 26), // "on_txt04RegNum_textChanged"
QT_MOC_LITERAL(33, 637, 29), // "on_txt06SlaveAddr_textChanged"
QT_MOC_LITERAL(34, 667, 27), // "on_txt06RegAddr_textChanged"
QT_MOC_LITERAL(35, 695, 25), // "on_txt06Value_textChanged"
QT_MOC_LITERAL(36, 721, 29), // "on_txt10SlaveAddr_textChanged"
QT_MOC_LITERAL(37, 751, 27), // "on_txt10RegAddr_textChanged"
QT_MOC_LITERAL(38, 779, 26), // "on_txt10RegNum_textChanged"
QT_MOC_LITERAL(39, 806, 23), // "on_btnOtherSend_clicked"
QT_MOC_LITERAL(40, 830, 20), // "on_btn03Send_clicked"
QT_MOC_LITERAL(41, 851, 20), // "on_btn04Send_clicked"
QT_MOC_LITERAL(42, 872, 20), // "on_btn06Send_clicked"
QT_MOC_LITERAL(43, 893, 20), // "on_btn10Send_clicked"
QT_MOC_LITERAL(44, 914, 20), // "on_btnExpand_clicked"
QT_MOC_LITERAL(45, 935, 25), // "on_actionRename_triggered"
QT_MOC_LITERAL(46, 961, 18), // "on_btnPlus_clicked"
QT_MOC_LITERAL(47, 980, 22), // "on_btnSubtract_clicked"
QT_MOC_LITERAL(48, 1003, 21), // "on_pushButton_clicked"
QT_MOC_LITERAL(49, 1025, 28), // "on_actionModbusPro_triggered"
QT_MOC_LITERAL(50, 1054, 24) // "on_actionStyle_triggered"

    },
    "ModbusMaster\0signal_writtenData\0\0txBuf\0"
    "signal_cmd01HProtocal\0signal_cmd03HProtocal\0"
    "signal_cmd05HProtocal\0signal_cmd06HProtocal\0"
    "signal_cmd10HProtocal\0slots_RxCallback\0"
    "slots_errorHandler\0QSerialPort::SerialPortError\0"
    "error\0slots_waitForCRC\0slots_cmd01HProtocal\0"
    "slots_cmd03HProtocal\0slots_cmd05HProtocal\0"
    "slots_cmd06HProtocal\0slots_cmd10HProtocal\0"
    "slots_showBtnRightClickMenu\0pos\0"
    "slots_clicktoSend\0slots_singleShot\0"
    "on_btnRefresh_clicked\0on_btnOpenPort_clicked\0"
    "checked\0on_txt03SlaveAddr_textChanged\0"
    "arg1\0on_txt03RegAddr_textChanged\0"
    "on_txt03RegNum_textChanged\0"
    "on_txt04SlaveAddr_textChanged\0"
    "on_txt04RegAddr_textChanged\0"
    "on_txt04RegNum_textChanged\0"
    "on_txt06SlaveAddr_textChanged\0"
    "on_txt06RegAddr_textChanged\0"
    "on_txt06Value_textChanged\0"
    "on_txt10SlaveAddr_textChanged\0"
    "on_txt10RegAddr_textChanged\0"
    "on_txt10RegNum_textChanged\0"
    "on_btnOtherSend_clicked\0on_btn03Send_clicked\0"
    "on_btn04Send_clicked\0on_btn06Send_clicked\0"
    "on_btn10Send_clicked\0on_btnExpand_clicked\0"
    "on_actionRename_triggered\0on_btnPlus_clicked\0"
    "on_btnSubtract_clicked\0on_pushButton_clicked\0"
    "on_actionModbusPro_triggered\0"
    "on_actionStyle_triggered"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ModbusMaster[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      43,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  229,    2, 0x06 /* Public */,
       4,    0,  232,    2, 0x06 /* Public */,
       5,    0,  233,    2, 0x06 /* Public */,
       6,    0,  234,    2, 0x06 /* Public */,
       7,    0,  235,    2, 0x06 /* Public */,
       8,    0,  236,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       9,    0,  237,    2, 0x0a /* Public */,
      10,    1,  238,    2, 0x0a /* Public */,
      13,    0,  241,    2, 0x0a /* Public */,
      14,    0,  242,    2, 0x0a /* Public */,
      15,    0,  243,    2, 0x0a /* Public */,
      16,    0,  244,    2, 0x0a /* Public */,
      17,    0,  245,    2, 0x0a /* Public */,
      18,    0,  246,    2, 0x0a /* Public */,
      19,    1,  247,    2, 0x0a /* Public */,
      21,    0,  250,    2, 0x0a /* Public */,
      22,    0,  251,    2, 0x0a /* Public */,
      23,    0,  252,    2, 0x08 /* Private */,
      24,    1,  253,    2, 0x08 /* Private */,
      26,    1,  256,    2, 0x08 /* Private */,
      28,    1,  259,    2, 0x08 /* Private */,
      29,    1,  262,    2, 0x08 /* Private */,
      30,    1,  265,    2, 0x08 /* Private */,
      31,    1,  268,    2, 0x08 /* Private */,
      32,    1,  271,    2, 0x08 /* Private */,
      33,    1,  274,    2, 0x08 /* Private */,
      34,    1,  277,    2, 0x08 /* Private */,
      35,    1,  280,    2, 0x08 /* Private */,
      36,    1,  283,    2, 0x08 /* Private */,
      37,    1,  286,    2, 0x08 /* Private */,
      38,    1,  289,    2, 0x08 /* Private */,
      39,    0,  292,    2, 0x08 /* Private */,
      40,    0,  293,    2, 0x08 /* Private */,
      41,    0,  294,    2, 0x08 /* Private */,
      42,    0,  295,    2, 0x08 /* Private */,
      43,    0,  296,    2, 0x08 /* Private */,
      44,    0,  297,    2, 0x08 /* Private */,
      45,    0,  298,    2, 0x08 /* Private */,
      46,    0,  299,    2, 0x08 /* Private */,
      47,    0,  300,    2, 0x08 /* Private */,
      48,    1,  301,    2, 0x08 /* Private */,
      49,    0,  304,    2, 0x08 /* Private */,
      50,    0,  305,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::QByteArray,    3,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QPoint,   20,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   25,
    QMetaType::Void, QMetaType::QString,   27,
    QMetaType::Void, QMetaType::QString,   27,
    QMetaType::Void, QMetaType::QString,   27,
    QMetaType::Void, QMetaType::QString,   27,
    QMetaType::Void, QMetaType::QString,   27,
    QMetaType::Void, QMetaType::QString,   27,
    QMetaType::Void, QMetaType::QString,   27,
    QMetaType::Void, QMetaType::QString,   27,
    QMetaType::Void, QMetaType::QString,   27,
    QMetaType::Void, QMetaType::QString,   27,
    QMetaType::Void, QMetaType::QString,   27,
    QMetaType::Void, QMetaType::QString,   27,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   25,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void ModbusMaster::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ModbusMaster *_t = static_cast<ModbusMaster *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->signal_writtenData((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 1: _t->signal_cmd01HProtocal(); break;
        case 2: _t->signal_cmd03HProtocal(); break;
        case 3: _t->signal_cmd05HProtocal(); break;
        case 4: _t->signal_cmd06HProtocal(); break;
        case 5: _t->signal_cmd10HProtocal(); break;
        case 6: _t->slots_RxCallback(); break;
        case 7: _t->slots_errorHandler((*reinterpret_cast< QSerialPort::SerialPortError(*)>(_a[1]))); break;
        case 8: _t->slots_waitForCRC(); break;
        case 9: _t->slots_cmd01HProtocal(); break;
        case 10: _t->slots_cmd03HProtocal(); break;
        case 11: _t->slots_cmd05HProtocal(); break;
        case 12: _t->slots_cmd06HProtocal(); break;
        case 13: _t->slots_cmd10HProtocal(); break;
        case 14: _t->slots_showBtnRightClickMenu((*reinterpret_cast< const QPoint(*)>(_a[1]))); break;
        case 15: _t->slots_clicktoSend(); break;
        case 16: _t->slots_singleShot(); break;
        case 17: _t->on_btnRefresh_clicked(); break;
        case 18: _t->on_btnOpenPort_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 19: _t->on_txt03SlaveAddr_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 20: _t->on_txt03RegAddr_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 21: _t->on_txt03RegNum_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 22: _t->on_txt04SlaveAddr_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 23: _t->on_txt04RegAddr_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 24: _t->on_txt04RegNum_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 25: _t->on_txt06SlaveAddr_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 26: _t->on_txt06RegAddr_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 27: _t->on_txt06Value_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 28: _t->on_txt10SlaveAddr_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 29: _t->on_txt10RegAddr_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 30: _t->on_txt10RegNum_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 31: _t->on_btnOtherSend_clicked(); break;
        case 32: _t->on_btn03Send_clicked(); break;
        case 33: _t->on_btn04Send_clicked(); break;
        case 34: _t->on_btn06Send_clicked(); break;
        case 35: _t->on_btn10Send_clicked(); break;
        case 36: _t->on_btnExpand_clicked(); break;
        case 37: _t->on_actionRename_triggered(); break;
        case 38: _t->on_btnPlus_clicked(); break;
        case 39: _t->on_btnSubtract_clicked(); break;
        case 40: _t->on_pushButton_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 41: _t->on_actionModbusPro_triggered(); break;
        case 42: _t->on_actionStyle_triggered(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (ModbusMaster::*_t)(QByteArray );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ModbusMaster::signal_writtenData)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (ModbusMaster::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ModbusMaster::signal_cmd01HProtocal)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (ModbusMaster::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ModbusMaster::signal_cmd03HProtocal)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (ModbusMaster::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ModbusMaster::signal_cmd05HProtocal)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (ModbusMaster::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ModbusMaster::signal_cmd06HProtocal)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (ModbusMaster::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ModbusMaster::signal_cmd10HProtocal)) {
                *result = 5;
                return;
            }
        }
    }
}

const QMetaObject ModbusMaster::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_ModbusMaster.data,
      qt_meta_data_ModbusMaster,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *ModbusMaster::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ModbusMaster::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_ModbusMaster.stringdata0))
        return static_cast<void*>(const_cast< ModbusMaster*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int ModbusMaster::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 43)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 43;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 43)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 43;
    }
    return _id;
}

// SIGNAL 0
void ModbusMaster::signal_writtenData(QByteArray _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void ModbusMaster::signal_cmd01HProtocal()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void ModbusMaster::signal_cmd03HProtocal()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void ModbusMaster::signal_cmd05HProtocal()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void ModbusMaster::signal_cmd06HProtocal()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void ModbusMaster::signal_cmd10HProtocal()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
