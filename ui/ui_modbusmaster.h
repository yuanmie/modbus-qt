/********************************************************************************
** Form generated from reading UI file 'modbusmaster.ui'
**
** Created by: Qt User Interface Compiler version 5.9.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MODBUSMASTER_H
#define UI_MODBUSMASTER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ModbusMaster
{
public:
    QAction *actionModbusPro;
    QAction *actionHelp;
    QAction *actionRename;
    QAction *actionStyle;
    QWidget *centralWidget;
    QGridLayout *gridLayout_4;
    QSplitter *splitter_4;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_3;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_5;
    QSplitter *splitter_2;
    QRadioButton *rdbRTU;
    QRadioButton *rdbASCII;
    QSplitter *splitter;
    QPushButton *btnExpand;
    QToolButton *btnPlus;
    QToolButton *btnSubtract;
    QCheckBox *ckbInsertCRC;
    QLabel *lblTips;
    QPushButton *pushButton;
    QToolButton *toolButton;
    QSpacerItem *verticalSpacer;
    QGroupBox *QGBSerialConfig;
    QGridLayout *gridLayout_6;
    QPushButton *btnRefresh;
    QPushButton *btnOpenPort;
    QGridLayout *gridLayout;
    QLabel *lblPortNum;
    QComboBox *cbbPortNum;
    QLabel *lblBaudRate;
    QComboBox *cbbBaud;
    QLabel *lblDataBit;
    QComboBox *cbbDataBit;
    QLabel *lblVerify;
    QComboBox *cbbVerify;
    QComboBox *cbbStopBit;
    QLabel *lblStopBit;
    QSplitter *splitter_3;
    QGroupBox *groupBox_8;
    QGridLayout *gridLayout_2;
    QLineEdit *lineEdit_10H_13;
    QLineEdit *lineEdit_10H_09;
    QLineEdit *lineEdit_10H_18;
    QLineEdit *lineEdit_10H_08;
    QLineEdit *lineEdit_10H_10;
    QLineEdit *lineEdit_10H_22;
    QLineEdit *lineEdit_10H_24;
    QLineEdit *lineEdit_10H_25;
    QLineEdit *lineEdit_10H_21;
    QLineEdit *lineEdit_10H_19;
    QLineEdit *lineEdit_10H_15;
    QLineEdit *lineEdit_10H_11;
    QLineEdit *lineEdit_10H_17;
    QLineEdit *lineEdit_10H_27;
    QLineEdit *lineEdit_10H_14;
    QLineEdit *txt03SlaveAddr;
    QLineEdit *lineEdit_10H_20;
    QLineEdit *txt03RegAddr;
    QLineEdit *txt06SlaveAddr;
    QPushButton *btn03Send;
    QLineEdit *txt03RegNum;
    QLineEdit *txt10RegNum;
    QLineEdit *txt04SlaveAddr;
    QLineEdit *lineEdit_10H_01;
    QLineEdit *txt06Value;
    QLineEdit *txt06RegAddr;
    QLineEdit *txt04RegAddr;
    QLineEdit *lineEdit_10H_02;
    QFrame *line_6;
    QLineEdit *txt10SlaveAddr;
    QPushButton *btn04Send;
    QLineEdit *txt10Value;
    QLineEdit *lineEdit_10H_03;
    QPushButton *btn10Send;
    QFrame *line_3;
    QLineEdit *txt04RegNum;
    QLineEdit *txt10RegAddr;
    QPushButton *btn06Send;
    QLineEdit *txt03Read;
    QFrame *line_2;
    QLineEdit *lineEdit_10H_23;
    QLineEdit *txt04Read;
    QLineEdit *txtOtherValue;
    QLineEdit *lineEdit_10H_05;
    QLineEdit *lineEdit_10H_26;
    QLineEdit *lineEdit_10H_04;
    QLineEdit *lineEdit_10H_07;
    QFrame *line_5;
    QLineEdit *lineEdit_10H_16;
    QLineEdit *lineEdit_10H_06;
    QPushButton *btnOtherSend;
    QLineEdit *lineEdit_10H_12;
    QLabel *label_5;
    QLabel *label;
    QLabel *label_4;
    QLabel *label_7;
    QLabel *label_3;
    QLabel *label_9;
    QLabel *label_6;
    QLabel *label_8;
    QLabel *label_11;
    QLineEdit *lineEdit_10H_30;
    QLineEdit *lineEdit_10H_28;
    QLabel *label_10;
    QLineEdit *lineEdit_10H_29;
    QLabel *label_2;
    QTextEdit *txtMessage;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QToolBar *toolBar;
    QButtonGroup *bgpMSS;

    void setupUi(QMainWindow *ModbusMaster)
    {
        if (ModbusMaster->objectName().isEmpty())
            ModbusMaster->setObjectName(QStringLiteral("ModbusMaster"));
        ModbusMaster->resize(1787, 760);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(ModbusMaster->sizePolicy().hasHeightForWidth());
        ModbusMaster->setSizePolicy(sizePolicy);
        ModbusMaster->setMinimumSize(QSize(593, 0));
        actionModbusPro = new QAction(ModbusMaster);
        actionModbusPro->setObjectName(QStringLiteral("actionModbusPro"));
        QIcon icon;
        icon.addFile(QStringLiteral(":/general/general/protocol.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionModbusPro->setIcon(icon);
        actionHelp = new QAction(ModbusMaster);
        actionHelp->setObjectName(QStringLiteral("actionHelp"));
        actionRename = new QAction(ModbusMaster);
        actionRename->setObjectName(QStringLiteral("actionRename"));
        actionStyle = new QAction(ModbusMaster);
        actionStyle->setObjectName(QStringLiteral("actionStyle"));
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/general/general/Style.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionStyle->setIcon(icon1);
        centralWidget = new QWidget(ModbusMaster);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout_4 = new QGridLayout(centralWidget);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        splitter_4 = new QSplitter(centralWidget);
        splitter_4->setObjectName(QStringLiteral("splitter_4"));
        splitter_4->setOrientation(Qt::Horizontal);
        splitter_4->setChildrenCollapsible(false);
        groupBox = new QGroupBox(splitter_4);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(groupBox->sizePolicy().hasHeightForWidth());
        groupBox->setSizePolicy(sizePolicy1);
        groupBox->setMinimumSize(QSize(524, 442));
        gridLayout_3 = new QGridLayout(groupBox);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        groupBox_2 = new QGroupBox(groupBox);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        QSizePolicy sizePolicy2(QSizePolicy::Minimum, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(groupBox_2->sizePolicy().hasHeightForWidth());
        groupBox_2->setSizePolicy(sizePolicy2);
        groupBox_2->setMinimumSize(QSize(0, 0));
        groupBox_2->setMaximumSize(QSize(500, 16777215));
        gridLayout_5 = new QGridLayout(groupBox_2);
        gridLayout_5->setSpacing(2);
        gridLayout_5->setContentsMargins(11, 11, 11, 11);
        gridLayout_5->setObjectName(QStringLiteral("gridLayout_5"));
        gridLayout_5->setContentsMargins(3, 3, 3, 3);
        splitter_2 = new QSplitter(groupBox_2);
        splitter_2->setObjectName(QStringLiteral("splitter_2"));
        splitter_2->setOrientation(Qt::Horizontal);
        splitter_2->setHandleWidth(0);
        rdbRTU = new QRadioButton(splitter_2);
        bgpMSS = new QButtonGroup(ModbusMaster);
        bgpMSS->setObjectName(QStringLiteral("bgpMSS"));
        bgpMSS->addButton(rdbRTU);
        rdbRTU->setObjectName(QStringLiteral("rdbRTU"));
        QSizePolicy sizePolicy3(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy3.setHorizontalStretch(1);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(rdbRTU->sizePolicy().hasHeightForWidth());
        rdbRTU->setSizePolicy(sizePolicy3);
        rdbRTU->setChecked(true);
        splitter_2->addWidget(rdbRTU);
        rdbASCII = new QRadioButton(splitter_2);
        bgpMSS->addButton(rdbASCII);
        rdbASCII->setObjectName(QStringLiteral("rdbASCII"));
        sizePolicy3.setHeightForWidth(rdbASCII->sizePolicy().hasHeightForWidth());
        rdbASCII->setSizePolicy(sizePolicy3);
        splitter_2->addWidget(rdbASCII);

        gridLayout_5->addWidget(splitter_2, 2, 0, 1, 2);

        splitter = new QSplitter(groupBox_2);
        splitter->setObjectName(QStringLiteral("splitter"));
        splitter->setOrientation(Qt::Horizontal);
        splitter->setHandleWidth(0);
        btnExpand = new QPushButton(splitter);
        btnExpand->setObjectName(QStringLiteral("btnExpand"));
        QSizePolicy sizePolicy4(QSizePolicy::Expanding, QSizePolicy::Minimum);
        sizePolicy4.setHorizontalStretch(3);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(btnExpand->sizePolicy().hasHeightForWidth());
        btnExpand->setSizePolicy(sizePolicy4);
        splitter->addWidget(btnExpand);
        btnPlus = new QToolButton(splitter);
        btnPlus->setObjectName(QStringLiteral("btnPlus"));
        splitter->addWidget(btnPlus);
        btnSubtract = new QToolButton(splitter);
        btnSubtract->setObjectName(QStringLiteral("btnSubtract"));
        splitter->addWidget(btnSubtract);

        gridLayout_5->addWidget(splitter, 0, 0, 1, 2);

        ckbInsertCRC = new QCheckBox(groupBox_2);
        ckbInsertCRC->setObjectName(QStringLiteral("ckbInsertCRC"));
        ckbInsertCRC->setChecked(true);

        gridLayout_5->addWidget(ckbInsertCRC, 1, 0, 1, 2);

        lblTips = new QLabel(groupBox_2);
        lblTips->setObjectName(QStringLiteral("lblTips"));
        lblTips->setMinimumSize(QSize(0, 80));
        lblTips->setWordWrap(true);

        gridLayout_5->addWidget(lblTips, 4, 0, 1, 2);

        pushButton = new QPushButton(groupBox_2);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        QSizePolicy sizePolicy5(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy5.setHorizontalStretch(4);
        sizePolicy5.setVerticalStretch(0);
        sizePolicy5.setHeightForWidth(pushButton->sizePolicy().hasHeightForWidth());
        pushButton->setSizePolicy(sizePolicy5);
        pushButton->setMinimumSize(QSize(0, 30));
        QIcon icon2;
        icon2.addFile(QStringLiteral(":/general/general/ing10.ico"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton->setIcon(icon2);
        pushButton->setIconSize(QSize(24, 24));
        pushButton->setCheckable(true);

        gridLayout_5->addWidget(pushButton, 3, 1, 1, 1);

        toolButton = new QToolButton(groupBox_2);
        toolButton->setObjectName(QStringLiteral("toolButton"));
        QSizePolicy sizePolicy6(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy6.setHorizontalStretch(1);
        sizePolicy6.setVerticalStretch(0);
        sizePolicy6.setHeightForWidth(toolButton->sizePolicy().hasHeightForWidth());
        toolButton->setSizePolicy(sizePolicy6);
        toolButton->setMinimumSize(QSize(0, 30));
        QIcon icon3;
        icon3.addFile(QStringLiteral(":/action/action/clear.png"), QSize(), QIcon::Normal, QIcon::Off);
        toolButton->setIcon(icon3);
        toolButton->setIconSize(QSize(24, 24));

        gridLayout_5->addWidget(toolButton, 3, 0, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_5->addItem(verticalSpacer, 5, 0, 1, 2);


        gridLayout_3->addWidget(groupBox_2, 1, 0, 1, 1);

        QGBSerialConfig = new QGroupBox(groupBox);
        QGBSerialConfig->setObjectName(QStringLiteral("QGBSerialConfig"));
        QGBSerialConfig->setEnabled(true);
        QSizePolicy sizePolicy7(QSizePolicy::Expanding, QSizePolicy::Maximum);
        sizePolicy7.setHorizontalStretch(0);
        sizePolicy7.setVerticalStretch(0);
        sizePolicy7.setHeightForWidth(QGBSerialConfig->sizePolicy().hasHeightForWidth());
        QGBSerialConfig->setSizePolicy(sizePolicy7);
        QGBSerialConfig->setMinimumSize(QSize(171, 211));
        QGBSerialConfig->setMaximumSize(QSize(500, 16777215));
        QGBSerialConfig->setStyleSheet(QStringLiteral(""));
        QGBSerialConfig->setFlat(false);
        QGBSerialConfig->setCheckable(false);
        gridLayout_6 = new QGridLayout(QGBSerialConfig);
        gridLayout_6->setSpacing(2);
        gridLayout_6->setContentsMargins(11, 11, 11, 11);
        gridLayout_6->setObjectName(QStringLiteral("gridLayout_6"));
        gridLayout_6->setContentsMargins(3, 3, 3, 3);
        btnRefresh = new QPushButton(QGBSerialConfig);
        btnRefresh->setObjectName(QStringLiteral("btnRefresh"));
        btnRefresh->setMaximumSize(QSize(60, 16777215));
        QIcon icon4;
        icon4.addFile(QStringLiteral(":/action/action/refresh.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnRefresh->setIcon(icon4);
        btnRefresh->setIconSize(QSize(32, 32));

        gridLayout_6->addWidget(btnRefresh, 1, 0, 1, 1);

        btnOpenPort = new QPushButton(QGBSerialConfig);
        btnOpenPort->setObjectName(QStringLiteral("btnOpenPort"));
        btnOpenPort->setEnabled(true);
        btnOpenPort->setStyleSheet(QStringLiteral(""));
        QIcon icon5;
        icon5.addFile(QStringLiteral(":/general/general/com_disconnect.png"), QSize(), QIcon::Normal, QIcon::Off);
        icon5.addFile(QStringLiteral(":/general/general/com_connect_1.png"), QSize(), QIcon::Normal, QIcon::On);
        btnOpenPort->setIcon(icon5);
        btnOpenPort->setIconSize(QSize(32, 32));
        btnOpenPort->setCheckable(true);
        btnOpenPort->setChecked(false);
        btnOpenPort->setAutoDefault(false);
        btnOpenPort->setFlat(false);

        gridLayout_6->addWidget(btnOpenPort, 1, 1, 1, 1);

        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setSizeConstraint(QLayout::SetMaximumSize);
        lblPortNum = new QLabel(QGBSerialConfig);
        lblPortNum->setObjectName(QStringLiteral("lblPortNum"));
        sizePolicy1.setHeightForWidth(lblPortNum->sizePolicy().hasHeightForWidth());
        lblPortNum->setSizePolicy(sizePolicy1);

        gridLayout->addWidget(lblPortNum, 0, 0, 1, 1);

        cbbPortNum = new QComboBox(QGBSerialConfig);
        cbbPortNum->setObjectName(QStringLiteral("cbbPortNum"));
        sizePolicy1.setHeightForWidth(cbbPortNum->sizePolicy().hasHeightForWidth());
        cbbPortNum->setSizePolicy(sizePolicy1);
        QFont font;
        font.setPointSize(16);
        cbbPortNum->setFont(font);
        cbbPortNum->setMaxVisibleItems(20);
        cbbPortNum->setSizeAdjustPolicy(QComboBox::AdjustToContents);
        cbbPortNum->setMinimumContentsLength(0);

        gridLayout->addWidget(cbbPortNum, 0, 1, 1, 1);

        lblBaudRate = new QLabel(QGBSerialConfig);
        lblBaudRate->setObjectName(QStringLiteral("lblBaudRate"));
        sizePolicy1.setHeightForWidth(lblBaudRate->sizePolicy().hasHeightForWidth());
        lblBaudRate->setSizePolicy(sizePolicy1);

        gridLayout->addWidget(lblBaudRate, 1, 0, 1, 1);

        cbbBaud = new QComboBox(QGBSerialConfig);
        cbbBaud->setObjectName(QStringLiteral("cbbBaud"));
        sizePolicy1.setHeightForWidth(cbbBaud->sizePolicy().hasHeightForWidth());
        cbbBaud->setSizePolicy(sizePolicy1);
        cbbBaud->setFont(font);
        cbbBaud->setSizeAdjustPolicy(QComboBox::AdjustToContents);

        gridLayout->addWidget(cbbBaud, 1, 1, 1, 1);

        lblDataBit = new QLabel(QGBSerialConfig);
        lblDataBit->setObjectName(QStringLiteral("lblDataBit"));
        sizePolicy1.setHeightForWidth(lblDataBit->sizePolicy().hasHeightForWidth());
        lblDataBit->setSizePolicy(sizePolicy1);

        gridLayout->addWidget(lblDataBit, 2, 0, 1, 1);

        cbbDataBit = new QComboBox(QGBSerialConfig);
        cbbDataBit->setObjectName(QStringLiteral("cbbDataBit"));
        sizePolicy1.setHeightForWidth(cbbDataBit->sizePolicy().hasHeightForWidth());
        cbbDataBit->setSizePolicy(sizePolicy1);
        cbbDataBit->setFont(font);
        cbbDataBit->setSizeAdjustPolicy(QComboBox::AdjustToContents);

        gridLayout->addWidget(cbbDataBit, 2, 1, 1, 1);

        lblVerify = new QLabel(QGBSerialConfig);
        lblVerify->setObjectName(QStringLiteral("lblVerify"));
        sizePolicy1.setHeightForWidth(lblVerify->sizePolicy().hasHeightForWidth());
        lblVerify->setSizePolicy(sizePolicy1);

        gridLayout->addWidget(lblVerify, 3, 0, 1, 1);

        cbbVerify = new QComboBox(QGBSerialConfig);
        cbbVerify->setObjectName(QStringLiteral("cbbVerify"));
        sizePolicy1.setHeightForWidth(cbbVerify->sizePolicy().hasHeightForWidth());
        cbbVerify->setSizePolicy(sizePolicy1);
        cbbVerify->setFont(font);
        cbbVerify->setSizeAdjustPolicy(QComboBox::AdjustToContents);

        gridLayout->addWidget(cbbVerify, 3, 1, 1, 1);

        cbbStopBit = new QComboBox(QGBSerialConfig);
        cbbStopBit->setObjectName(QStringLiteral("cbbStopBit"));
        sizePolicy1.setHeightForWidth(cbbStopBit->sizePolicy().hasHeightForWidth());
        cbbStopBit->setSizePolicy(sizePolicy1);
        cbbStopBit->setFont(font);
        cbbStopBit->setSizeAdjustPolicy(QComboBox::AdjustToContents);

        gridLayout->addWidget(cbbStopBit, 4, 1, 1, 1);

        lblStopBit = new QLabel(QGBSerialConfig);
        lblStopBit->setObjectName(QStringLiteral("lblStopBit"));
        sizePolicy1.setHeightForWidth(lblStopBit->sizePolicy().hasHeightForWidth());
        lblStopBit->setSizePolicy(sizePolicy1);

        gridLayout->addWidget(lblStopBit, 4, 0, 1, 1);


        gridLayout_6->addLayout(gridLayout, 0, 0, 1, 2);


        gridLayout_3->addWidget(QGBSerialConfig, 0, 0, 1, 1);

        splitter_3 = new QSplitter(groupBox);
        splitter_3->setObjectName(QStringLiteral("splitter_3"));
        splitter_3->setOrientation(Qt::Vertical);
        groupBox_8 = new QGroupBox(splitter_3);
        groupBox_8->setObjectName(QStringLiteral("groupBox_8"));
        QSizePolicy sizePolicy8(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy8.setHorizontalStretch(0);
        sizePolicy8.setVerticalStretch(0);
        sizePolicy8.setHeightForWidth(groupBox_8->sizePolicy().hasHeightForWidth());
        groupBox_8->setSizePolicy(sizePolicy8);
        groupBox_8->setMinimumSize(QSize(0, 245));
        gridLayout_2 = new QGridLayout(groupBox_8);
        gridLayout_2->setSpacing(2);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        gridLayout_2->setContentsMargins(3, 3, 3, 3);
        lineEdit_10H_13 = new QLineEdit(groupBox_8);
        lineEdit_10H_13->setObjectName(QStringLiteral("lineEdit_10H_13"));
        sizePolicy1.setHeightForWidth(lineEdit_10H_13->sizePolicy().hasHeightForWidth());
        lineEdit_10H_13->setSizePolicy(sizePolicy1);

        gridLayout_2->addWidget(lineEdit_10H_13, 19, 1, 1, 1);

        lineEdit_10H_09 = new QLineEdit(groupBox_8);
        lineEdit_10H_09->setObjectName(QStringLiteral("lineEdit_10H_09"));
        sizePolicy1.setHeightForWidth(lineEdit_10H_09->sizePolicy().hasHeightForWidth());
        lineEdit_10H_09->setSizePolicy(sizePolicy1);

        gridLayout_2->addWidget(lineEdit_10H_09, 17, 3, 1, 1);

        lineEdit_10H_18 = new QLineEdit(groupBox_8);
        lineEdit_10H_18->setObjectName(QStringLiteral("lineEdit_10H_18"));
        sizePolicy1.setHeightForWidth(lineEdit_10H_18->sizePolicy().hasHeightForWidth());
        lineEdit_10H_18->setSizePolicy(sizePolicy1);

        gridLayout_2->addWidget(lineEdit_10H_18, 20, 3, 1, 1);

        lineEdit_10H_08 = new QLineEdit(groupBox_8);
        lineEdit_10H_08->setObjectName(QStringLiteral("lineEdit_10H_08"));
        sizePolicy1.setHeightForWidth(lineEdit_10H_08->sizePolicy().hasHeightForWidth());
        lineEdit_10H_08->setSizePolicy(sizePolicy1);

        gridLayout_2->addWidget(lineEdit_10H_08, 17, 2, 1, 1);

        lineEdit_10H_10 = new QLineEdit(groupBox_8);
        lineEdit_10H_10->setObjectName(QStringLiteral("lineEdit_10H_10"));
        sizePolicy1.setHeightForWidth(lineEdit_10H_10->sizePolicy().hasHeightForWidth());
        lineEdit_10H_10->setSizePolicy(sizePolicy1);

        gridLayout_2->addWidget(lineEdit_10H_10, 18, 1, 1, 1);

        lineEdit_10H_22 = new QLineEdit(groupBox_8);
        lineEdit_10H_22->setObjectName(QStringLiteral("lineEdit_10H_22"));
        sizePolicy1.setHeightForWidth(lineEdit_10H_22->sizePolicy().hasHeightForWidth());
        lineEdit_10H_22->setSizePolicy(sizePolicy1);

        gridLayout_2->addWidget(lineEdit_10H_22, 22, 1, 1, 1);

        lineEdit_10H_24 = new QLineEdit(groupBox_8);
        lineEdit_10H_24->setObjectName(QStringLiteral("lineEdit_10H_24"));
        sizePolicy1.setHeightForWidth(lineEdit_10H_24->sizePolicy().hasHeightForWidth());
        lineEdit_10H_24->setSizePolicy(sizePolicy1);

        gridLayout_2->addWidget(lineEdit_10H_24, 22, 3, 1, 1);

        lineEdit_10H_25 = new QLineEdit(groupBox_8);
        lineEdit_10H_25->setObjectName(QStringLiteral("lineEdit_10H_25"));
        sizePolicy1.setHeightForWidth(lineEdit_10H_25->sizePolicy().hasHeightForWidth());
        lineEdit_10H_25->setSizePolicy(sizePolicy1);

        gridLayout_2->addWidget(lineEdit_10H_25, 23, 1, 1, 1);

        lineEdit_10H_21 = new QLineEdit(groupBox_8);
        lineEdit_10H_21->setObjectName(QStringLiteral("lineEdit_10H_21"));
        sizePolicy1.setHeightForWidth(lineEdit_10H_21->sizePolicy().hasHeightForWidth());
        lineEdit_10H_21->setSizePolicy(sizePolicy1);

        gridLayout_2->addWidget(lineEdit_10H_21, 21, 3, 1, 1);

        lineEdit_10H_19 = new QLineEdit(groupBox_8);
        lineEdit_10H_19->setObjectName(QStringLiteral("lineEdit_10H_19"));
        sizePolicy1.setHeightForWidth(lineEdit_10H_19->sizePolicy().hasHeightForWidth());
        lineEdit_10H_19->setSizePolicy(sizePolicy1);

        gridLayout_2->addWidget(lineEdit_10H_19, 21, 1, 1, 1);

        lineEdit_10H_15 = new QLineEdit(groupBox_8);
        lineEdit_10H_15->setObjectName(QStringLiteral("lineEdit_10H_15"));
        sizePolicy1.setHeightForWidth(lineEdit_10H_15->sizePolicy().hasHeightForWidth());
        lineEdit_10H_15->setSizePolicy(sizePolicy1);

        gridLayout_2->addWidget(lineEdit_10H_15, 19, 3, 1, 1);

        lineEdit_10H_11 = new QLineEdit(groupBox_8);
        lineEdit_10H_11->setObjectName(QStringLiteral("lineEdit_10H_11"));
        sizePolicy1.setHeightForWidth(lineEdit_10H_11->sizePolicy().hasHeightForWidth());
        lineEdit_10H_11->setSizePolicy(sizePolicy1);

        gridLayout_2->addWidget(lineEdit_10H_11, 18, 2, 1, 1);

        lineEdit_10H_17 = new QLineEdit(groupBox_8);
        lineEdit_10H_17->setObjectName(QStringLiteral("lineEdit_10H_17"));
        sizePolicy1.setHeightForWidth(lineEdit_10H_17->sizePolicy().hasHeightForWidth());
        lineEdit_10H_17->setSizePolicy(sizePolicy1);

        gridLayout_2->addWidget(lineEdit_10H_17, 20, 2, 1, 1);

        lineEdit_10H_27 = new QLineEdit(groupBox_8);
        lineEdit_10H_27->setObjectName(QStringLiteral("lineEdit_10H_27"));
        sizePolicy1.setHeightForWidth(lineEdit_10H_27->sizePolicy().hasHeightForWidth());
        lineEdit_10H_27->setSizePolicy(sizePolicy1);

        gridLayout_2->addWidget(lineEdit_10H_27, 23, 3, 1, 1);

        lineEdit_10H_14 = new QLineEdit(groupBox_8);
        lineEdit_10H_14->setObjectName(QStringLiteral("lineEdit_10H_14"));
        sizePolicy1.setHeightForWidth(lineEdit_10H_14->sizePolicy().hasHeightForWidth());
        lineEdit_10H_14->setSizePolicy(sizePolicy1);

        gridLayout_2->addWidget(lineEdit_10H_14, 19, 2, 1, 1);

        txt03SlaveAddr = new QLineEdit(groupBox_8);
        txt03SlaveAddr->setObjectName(QStringLiteral("txt03SlaveAddr"));
        sizePolicy1.setHeightForWidth(txt03SlaveAddr->sizePolicy().hasHeightForWidth());
        txt03SlaveAddr->setSizePolicy(sizePolicy1);
        txt03SlaveAddr->setToolTipDuration(2000);

        gridLayout_2->addWidget(txt03SlaveAddr, 0, 1, 1, 1);

        lineEdit_10H_20 = new QLineEdit(groupBox_8);
        lineEdit_10H_20->setObjectName(QStringLiteral("lineEdit_10H_20"));
        sizePolicy1.setHeightForWidth(lineEdit_10H_20->sizePolicy().hasHeightForWidth());
        lineEdit_10H_20->setSizePolicy(sizePolicy1);

        gridLayout_2->addWidget(lineEdit_10H_20, 21, 2, 1, 1);

        txt03RegAddr = new QLineEdit(groupBox_8);
        txt03RegAddr->setObjectName(QStringLiteral("txt03RegAddr"));
        sizePolicy1.setHeightForWidth(txt03RegAddr->sizePolicy().hasHeightForWidth());
        txt03RegAddr->setSizePolicy(sizePolicy1);
        txt03RegAddr->setToolTipDuration(2000);

        gridLayout_2->addWidget(txt03RegAddr, 0, 2, 1, 1);

        txt06SlaveAddr = new QLineEdit(groupBox_8);
        txt06SlaveAddr->setObjectName(QStringLiteral("txt06SlaveAddr"));
        sizePolicy1.setHeightForWidth(txt06SlaveAddr->sizePolicy().hasHeightForWidth());
        txt06SlaveAddr->setSizePolicy(sizePolicy1);
        txt06SlaveAddr->setToolTipDuration(2000);

        gridLayout_2->addWidget(txt06SlaveAddr, 8, 1, 1, 1);

        btn03Send = new QPushButton(groupBox_8);
        btn03Send->setObjectName(QStringLiteral("btn03Send"));
        QSizePolicy sizePolicy9(QSizePolicy::Minimum, QSizePolicy::Expanding);
        sizePolicy9.setHorizontalStretch(0);
        sizePolicy9.setVerticalStretch(0);
        sizePolicy9.setHeightForWidth(btn03Send->sizePolicy().hasHeightForWidth());
        btn03Send->setSizePolicy(sizePolicy9);
        btn03Send->setMaximumSize(QSize(61, 16777215));

        gridLayout_2->addWidget(btn03Send, 0, 0, 1, 1);

        txt03RegNum = new QLineEdit(groupBox_8);
        txt03RegNum->setObjectName(QStringLiteral("txt03RegNum"));
        sizePolicy1.setHeightForWidth(txt03RegNum->sizePolicy().hasHeightForWidth());
        txt03RegNum->setSizePolicy(sizePolicy1);
        txt03RegNum->setToolTipDuration(2000);

        gridLayout_2->addWidget(txt03RegNum, 0, 3, 1, 2);

        txt10RegNum = new QLineEdit(groupBox_8);
        txt10RegNum->setObjectName(QStringLiteral("txt10RegNum"));
        sizePolicy1.setHeightForWidth(txt10RegNum->sizePolicy().hasHeightForWidth());
        txt10RegNum->setSizePolicy(sizePolicy1);
        txt10RegNum->setToolTipDuration(2000);

        gridLayout_2->addWidget(txt10RegNum, 11, 3, 1, 2);

        txt04SlaveAddr = new QLineEdit(groupBox_8);
        txt04SlaveAddr->setObjectName(QStringLiteral("txt04SlaveAddr"));
        sizePolicy1.setHeightForWidth(txt04SlaveAddr->sizePolicy().hasHeightForWidth());
        txt04SlaveAddr->setSizePolicy(sizePolicy1);
        txt04SlaveAddr->setToolTipDuration(2000);

        gridLayout_2->addWidget(txt04SlaveAddr, 3, 1, 1, 1);

        lineEdit_10H_01 = new QLineEdit(groupBox_8);
        lineEdit_10H_01->setObjectName(QStringLiteral("lineEdit_10H_01"));
        sizePolicy1.setHeightForWidth(lineEdit_10H_01->sizePolicy().hasHeightForWidth());
        lineEdit_10H_01->setSizePolicy(sizePolicy1);

        gridLayout_2->addWidget(lineEdit_10H_01, 15, 1, 1, 1);

        txt06Value = new QLineEdit(groupBox_8);
        txt06Value->setObjectName(QStringLiteral("txt06Value"));
        sizePolicy1.setHeightForWidth(txt06Value->sizePolicy().hasHeightForWidth());
        txt06Value->setSizePolicy(sizePolicy1);
        txt06Value->setToolTipDuration(2000);

        gridLayout_2->addWidget(txt06Value, 8, 3, 1, 2);

        txt06RegAddr = new QLineEdit(groupBox_8);
        txt06RegAddr->setObjectName(QStringLiteral("txt06RegAddr"));
        sizePolicy1.setHeightForWidth(txt06RegAddr->sizePolicy().hasHeightForWidth());
        txt06RegAddr->setSizePolicy(sizePolicy1);
        txt06RegAddr->setToolTipDuration(2000);

        gridLayout_2->addWidget(txt06RegAddr, 8, 2, 1, 1);

        txt04RegAddr = new QLineEdit(groupBox_8);
        txt04RegAddr->setObjectName(QStringLiteral("txt04RegAddr"));
        sizePolicy1.setHeightForWidth(txt04RegAddr->sizePolicy().hasHeightForWidth());
        txt04RegAddr->setSizePolicy(sizePolicy1);
        txt04RegAddr->setToolTipDuration(2000);

        gridLayout_2->addWidget(txt04RegAddr, 3, 2, 1, 1);

        lineEdit_10H_02 = new QLineEdit(groupBox_8);
        lineEdit_10H_02->setObjectName(QStringLiteral("lineEdit_10H_02"));
        sizePolicy1.setHeightForWidth(lineEdit_10H_02->sizePolicy().hasHeightForWidth());
        lineEdit_10H_02->setSizePolicy(sizePolicy1);

        gridLayout_2->addWidget(lineEdit_10H_02, 15, 2, 1, 1);

        line_6 = new QFrame(groupBox_8);
        line_6->setObjectName(QStringLiteral("line_6"));
        line_6->setFrameShape(QFrame::HLine);
        line_6->setFrameShadow(QFrame::Sunken);

        gridLayout_2->addWidget(line_6, 9, 0, 1, 5);

        txt10SlaveAddr = new QLineEdit(groupBox_8);
        txt10SlaveAddr->setObjectName(QStringLiteral("txt10SlaveAddr"));
        sizePolicy1.setHeightForWidth(txt10SlaveAddr->sizePolicy().hasHeightForWidth());
        txt10SlaveAddr->setSizePolicy(sizePolicy1);
        txt10SlaveAddr->setMinimumSize(QSize(35, 0));
        txt10SlaveAddr->setMaximumSize(QSize(16777215, 16777215));
        txt10SlaveAddr->setToolTipDuration(2000);

        gridLayout_2->addWidget(txt10SlaveAddr, 11, 1, 1, 1);

        btn04Send = new QPushButton(groupBox_8);
        btn04Send->setObjectName(QStringLiteral("btn04Send"));
        sizePolicy9.setHeightForWidth(btn04Send->sizePolicy().hasHeightForWidth());
        btn04Send->setSizePolicy(sizePolicy9);
        btn04Send->setMaximumSize(QSize(61, 16777215));

        gridLayout_2->addWidget(btn04Send, 3, 0, 1, 1);

        txt10Value = new QLineEdit(groupBox_8);
        txt10Value->setObjectName(QStringLiteral("txt10Value"));
        sizePolicy1.setHeightForWidth(txt10Value->sizePolicy().hasHeightForWidth());
        txt10Value->setSizePolicy(sizePolicy1);
        txt10Value->setToolTipDuration(2000);

        gridLayout_2->addWidget(txt10Value, 14, 1, 1, 4);

        lineEdit_10H_03 = new QLineEdit(groupBox_8);
        lineEdit_10H_03->setObjectName(QStringLiteral("lineEdit_10H_03"));
        sizePolicy1.setHeightForWidth(lineEdit_10H_03->sizePolicy().hasHeightForWidth());
        lineEdit_10H_03->setSizePolicy(sizePolicy1);

        gridLayout_2->addWidget(lineEdit_10H_03, 15, 3, 1, 1);

        btn10Send = new QPushButton(groupBox_8);
        btn10Send->setObjectName(QStringLiteral("btn10Send"));
        sizePolicy9.setHeightForWidth(btn10Send->sizePolicy().hasHeightForWidth());
        btn10Send->setSizePolicy(sizePolicy9);
        btn10Send->setMaximumSize(QSize(61, 16777215));

        gridLayout_2->addWidget(btn10Send, 11, 0, 1, 1);

        line_3 = new QFrame(groupBox_8);
        line_3->setObjectName(QStringLiteral("line_3"));
        line_3->setFrameShape(QFrame::HLine);
        line_3->setFrameShadow(QFrame::Sunken);

        gridLayout_2->addWidget(line_3, 7, 0, 1, 5);

        txt04RegNum = new QLineEdit(groupBox_8);
        txt04RegNum->setObjectName(QStringLiteral("txt04RegNum"));
        sizePolicy1.setHeightForWidth(txt04RegNum->sizePolicy().hasHeightForWidth());
        txt04RegNum->setSizePolicy(sizePolicy1);
        txt04RegNum->setToolTipDuration(2000);

        gridLayout_2->addWidget(txt04RegNum, 3, 3, 1, 1);

        txt10RegAddr = new QLineEdit(groupBox_8);
        txt10RegAddr->setObjectName(QStringLiteral("txt10RegAddr"));
        sizePolicy1.setHeightForWidth(txt10RegAddr->sizePolicy().hasHeightForWidth());
        txt10RegAddr->setSizePolicy(sizePolicy1);
        txt10RegAddr->setToolTipDuration(2000);

        gridLayout_2->addWidget(txt10RegAddr, 11, 2, 1, 1);

        btn06Send = new QPushButton(groupBox_8);
        btn06Send->setObjectName(QStringLiteral("btn06Send"));
        sizePolicy9.setHeightForWidth(btn06Send->sizePolicy().hasHeightForWidth());
        btn06Send->setSizePolicy(sizePolicy9);
        btn06Send->setMaximumSize(QSize(61, 16777215));

        gridLayout_2->addWidget(btn06Send, 8, 0, 1, 1);

        txt03Read = new QLineEdit(groupBox_8);
        txt03Read->setObjectName(QStringLiteral("txt03Read"));
        sizePolicy1.setHeightForWidth(txt03Read->sizePolicy().hasHeightForWidth());
        txt03Read->setSizePolicy(sizePolicy1);
        txt03Read->setMinimumSize(QSize(0, 0));
        txt03Read->setReadOnly(false);

        gridLayout_2->addWidget(txt03Read, 1, 1, 1, 4);

        line_2 = new QFrame(groupBox_8);
        line_2->setObjectName(QStringLiteral("line_2"));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);

        gridLayout_2->addWidget(line_2, 2, 0, 1, 5);

        lineEdit_10H_23 = new QLineEdit(groupBox_8);
        lineEdit_10H_23->setObjectName(QStringLiteral("lineEdit_10H_23"));
        sizePolicy1.setHeightForWidth(lineEdit_10H_23->sizePolicy().hasHeightForWidth());
        lineEdit_10H_23->setSizePolicy(sizePolicy1);

        gridLayout_2->addWidget(lineEdit_10H_23, 22, 2, 1, 1);

        txt04Read = new QLineEdit(groupBox_8);
        txt04Read->setObjectName(QStringLiteral("txt04Read"));
        sizePolicy1.setHeightForWidth(txt04Read->sizePolicy().hasHeightForWidth());
        txt04Read->setSizePolicy(sizePolicy1);
        txt04Read->setReadOnly(false);

        gridLayout_2->addWidget(txt04Read, 4, 1, 1, 3);

        txtOtherValue = new QLineEdit(groupBox_8);
        txtOtherValue->setObjectName(QStringLiteral("txtOtherValue"));
        sizePolicy1.setHeightForWidth(txtOtherValue->sizePolicy().hasHeightForWidth());
        txtOtherValue->setSizePolicy(sizePolicy1);
        txtOtherValue->setToolTipDuration(2000);

        gridLayout_2->addWidget(txtOtherValue, 26, 1, 1, 4);

        lineEdit_10H_05 = new QLineEdit(groupBox_8);
        lineEdit_10H_05->setObjectName(QStringLiteral("lineEdit_10H_05"));
        sizePolicy1.setHeightForWidth(lineEdit_10H_05->sizePolicy().hasHeightForWidth());
        lineEdit_10H_05->setSizePolicy(sizePolicy1);

        gridLayout_2->addWidget(lineEdit_10H_05, 16, 2, 1, 1);

        lineEdit_10H_26 = new QLineEdit(groupBox_8);
        lineEdit_10H_26->setObjectName(QStringLiteral("lineEdit_10H_26"));
        sizePolicy1.setHeightForWidth(lineEdit_10H_26->sizePolicy().hasHeightForWidth());
        lineEdit_10H_26->setSizePolicy(sizePolicy1);

        gridLayout_2->addWidget(lineEdit_10H_26, 23, 2, 1, 1);

        lineEdit_10H_04 = new QLineEdit(groupBox_8);
        lineEdit_10H_04->setObjectName(QStringLiteral("lineEdit_10H_04"));
        sizePolicy1.setHeightForWidth(lineEdit_10H_04->sizePolicy().hasHeightForWidth());
        lineEdit_10H_04->setSizePolicy(sizePolicy1);

        gridLayout_2->addWidget(lineEdit_10H_04, 16, 1, 1, 1);

        lineEdit_10H_07 = new QLineEdit(groupBox_8);
        lineEdit_10H_07->setObjectName(QStringLiteral("lineEdit_10H_07"));
        sizePolicy1.setHeightForWidth(lineEdit_10H_07->sizePolicy().hasHeightForWidth());
        lineEdit_10H_07->setSizePolicy(sizePolicy1);

        gridLayout_2->addWidget(lineEdit_10H_07, 17, 1, 1, 1);

        line_5 = new QFrame(groupBox_8);
        line_5->setObjectName(QStringLiteral("line_5"));
        line_5->setFrameShape(QFrame::HLine);
        line_5->setFrameShadow(QFrame::Sunken);

        gridLayout_2->addWidget(line_5, 25, 0, 1, 5);

        lineEdit_10H_16 = new QLineEdit(groupBox_8);
        lineEdit_10H_16->setObjectName(QStringLiteral("lineEdit_10H_16"));
        sizePolicy1.setHeightForWidth(lineEdit_10H_16->sizePolicy().hasHeightForWidth());
        lineEdit_10H_16->setSizePolicy(sizePolicy1);

        gridLayout_2->addWidget(lineEdit_10H_16, 20, 1, 1, 1);

        lineEdit_10H_06 = new QLineEdit(groupBox_8);
        lineEdit_10H_06->setObjectName(QStringLiteral("lineEdit_10H_06"));
        sizePolicy1.setHeightForWidth(lineEdit_10H_06->sizePolicy().hasHeightForWidth());
        lineEdit_10H_06->setSizePolicy(sizePolicy1);

        gridLayout_2->addWidget(lineEdit_10H_06, 16, 3, 1, 1);

        btnOtherSend = new QPushButton(groupBox_8);
        btnOtherSend->setObjectName(QStringLiteral("btnOtherSend"));
        sizePolicy9.setHeightForWidth(btnOtherSend->sizePolicy().hasHeightForWidth());
        btnOtherSend->setSizePolicy(sizePolicy9);
        btnOtherSend->setMaximumSize(QSize(61, 16777215));

        gridLayout_2->addWidget(btnOtherSend, 26, 0, 1, 1);

        lineEdit_10H_12 = new QLineEdit(groupBox_8);
        lineEdit_10H_12->setObjectName(QStringLiteral("lineEdit_10H_12"));
        sizePolicy1.setHeightForWidth(lineEdit_10H_12->sizePolicy().hasHeightForWidth());
        lineEdit_10H_12->setSizePolicy(sizePolicy1);

        gridLayout_2->addWidget(lineEdit_10H_12, 18, 3, 1, 1);

        label_5 = new QLabel(groupBox_8);
        label_5->setObjectName(QStringLiteral("label_5"));
        sizePolicy8.setHeightForWidth(label_5->sizePolicy().hasHeightForWidth());
        label_5->setSizePolicy(sizePolicy8);
        label_5->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_5, 18, 0, 1, 1);

        label = new QLabel(groupBox_8);
        label->setObjectName(QStringLiteral("label"));
        sizePolicy8.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy8);
        label->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label, 14, 0, 1, 1);

        label_4 = new QLabel(groupBox_8);
        label_4->setObjectName(QStringLiteral("label_4"));
        sizePolicy8.setHeightForWidth(label_4->sizePolicy().hasHeightForWidth());
        label_4->setSizePolicy(sizePolicy8);
        label_4->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_4, 17, 0, 1, 1);

        label_7 = new QLabel(groupBox_8);
        label_7->setObjectName(QStringLiteral("label_7"));
        sizePolicy8.setHeightForWidth(label_7->sizePolicy().hasHeightForWidth());
        label_7->setSizePolicy(sizePolicy8);
        label_7->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_7, 20, 0, 1, 1);

        label_3 = new QLabel(groupBox_8);
        label_3->setObjectName(QStringLiteral("label_3"));
        sizePolicy8.setHeightForWidth(label_3->sizePolicy().hasHeightForWidth());
        label_3->setSizePolicy(sizePolicy8);
        label_3->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_3, 16, 0, 1, 1);

        label_9 = new QLabel(groupBox_8);
        label_9->setObjectName(QStringLiteral("label_9"));
        sizePolicy8.setHeightForWidth(label_9->sizePolicy().hasHeightForWidth());
        label_9->setSizePolicy(sizePolicy8);
        label_9->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_9, 22, 0, 1, 1);

        label_6 = new QLabel(groupBox_8);
        label_6->setObjectName(QStringLiteral("label_6"));
        sizePolicy8.setHeightForWidth(label_6->sizePolicy().hasHeightForWidth());
        label_6->setSizePolicy(sizePolicy8);
        label_6->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_6, 19, 0, 1, 1);

        label_8 = new QLabel(groupBox_8);
        label_8->setObjectName(QStringLiteral("label_8"));
        sizePolicy8.setHeightForWidth(label_8->sizePolicy().hasHeightForWidth());
        label_8->setSizePolicy(sizePolicy8);
        label_8->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_8, 21, 0, 1, 1);

        label_11 = new QLabel(groupBox_8);
        label_11->setObjectName(QStringLiteral("label_11"));
        sizePolicy8.setHeightForWidth(label_11->sizePolicy().hasHeightForWidth());
        label_11->setSizePolicy(sizePolicy8);
        label_11->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_11, 24, 0, 1, 1);

        lineEdit_10H_30 = new QLineEdit(groupBox_8);
        lineEdit_10H_30->setObjectName(QStringLiteral("lineEdit_10H_30"));
        sizePolicy1.setHeightForWidth(lineEdit_10H_30->sizePolicy().hasHeightForWidth());
        lineEdit_10H_30->setSizePolicy(sizePolicy1);

        gridLayout_2->addWidget(lineEdit_10H_30, 24, 3, 1, 1);

        lineEdit_10H_28 = new QLineEdit(groupBox_8);
        lineEdit_10H_28->setObjectName(QStringLiteral("lineEdit_10H_28"));
        sizePolicy1.setHeightForWidth(lineEdit_10H_28->sizePolicy().hasHeightForWidth());
        lineEdit_10H_28->setSizePolicy(sizePolicy1);

        gridLayout_2->addWidget(lineEdit_10H_28, 24, 1, 1, 1);

        label_10 = new QLabel(groupBox_8);
        label_10->setObjectName(QStringLiteral("label_10"));
        sizePolicy8.setHeightForWidth(label_10->sizePolicy().hasHeightForWidth());
        label_10->setSizePolicy(sizePolicy8);
        label_10->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_10, 23, 0, 1, 1);

        lineEdit_10H_29 = new QLineEdit(groupBox_8);
        lineEdit_10H_29->setObjectName(QStringLiteral("lineEdit_10H_29"));
        sizePolicy1.setHeightForWidth(lineEdit_10H_29->sizePolicy().hasHeightForWidth());
        lineEdit_10H_29->setSizePolicy(sizePolicy1);

        gridLayout_2->addWidget(lineEdit_10H_29, 24, 2, 1, 1);

        label_2 = new QLabel(groupBox_8);
        label_2->setObjectName(QStringLiteral("label_2"));
        sizePolicy8.setHeightForWidth(label_2->sizePolicy().hasHeightForWidth());
        label_2->setSizePolicy(sizePolicy8);
        label_2->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_2, 15, 0, 1, 1);

        splitter_3->addWidget(groupBox_8);
        txtMessage = new QTextEdit(splitter_3);
        txtMessage->setObjectName(QStringLiteral("txtMessage"));
        sizePolicy1.setHeightForWidth(txtMessage->sizePolicy().hasHeightForWidth());
        txtMessage->setSizePolicy(sizePolicy1);
        txtMessage->setReadOnly(true);
        splitter_3->addWidget(txtMessage);

        gridLayout_3->addWidget(splitter_3, 0, 1, 2, 1);

        splitter_4->addWidget(groupBox);
        splitter_3->raise();
        QGBSerialConfig->raise();
        groupBox_2->raise();
        scrollArea = new QScrollArea(splitter_4);
        scrollArea->setObjectName(QStringLiteral("scrollArea"));
        scrollArea->setMinimumSize(QSize(300, 0));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QStringLiteral("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 298, 696));
        scrollArea->setWidget(scrollAreaWidgetContents);
        splitter_4->addWidget(scrollArea);

        gridLayout_4->addWidget(splitter_4, 0, 0, 1, 1);

        ModbusMaster->setCentralWidget(centralWidget);
        toolBar = new QToolBar(ModbusMaster);
        toolBar->setObjectName(QStringLiteral("toolBar"));
        ModbusMaster->addToolBar(Qt::TopToolBarArea, toolBar);
        QWidget::setTabOrder(cbbPortNum, cbbBaud);
        QWidget::setTabOrder(cbbBaud, cbbDataBit);
        QWidget::setTabOrder(cbbDataBit, cbbVerify);
        QWidget::setTabOrder(cbbVerify, cbbStopBit);
        QWidget::setTabOrder(cbbStopBit, btnRefresh);
        QWidget::setTabOrder(btnRefresh, btnOpenPort);
        QWidget::setTabOrder(btnOpenPort, btnExpand);
        QWidget::setTabOrder(btnExpand, btnPlus);
        QWidget::setTabOrder(btnPlus, btnSubtract);
        QWidget::setTabOrder(btnSubtract, ckbInsertCRC);
        QWidget::setTabOrder(ckbInsertCRC, rdbRTU);
        QWidget::setTabOrder(rdbRTU, rdbASCII);
        QWidget::setTabOrder(rdbASCII, txt10SlaveAddr);
        QWidget::setTabOrder(txt10SlaveAddr, txt10RegAddr);
        QWidget::setTabOrder(txt10RegAddr, txt10RegNum);
        QWidget::setTabOrder(txt10RegNum, btn10Send);

        toolBar->addAction(actionModbusPro);
        toolBar->addAction(actionStyle);

        retranslateUi(ModbusMaster);
        QObject::connect(toolButton, SIGNAL(clicked()), txtMessage, SLOT(clear()));

        btnOpenPort->setDefault(false);


        QMetaObject::connectSlotsByName(ModbusMaster);
    } // setupUi

    void retranslateUi(QMainWindow *ModbusMaster)
    {
        ModbusMaster->setWindowTitle(QApplication::translate("ModbusMaster", "ModbusTool", Q_NULLPTR));
        actionModbusPro->setText(QApplication::translate("ModbusMaster", "Modbus\345\215\217\350\256\256", Q_NULLPTR));
        actionHelp->setText(QApplication::translate("ModbusMaster", "\345\270\256\345\212\251\350\257\264\346\230\216", Q_NULLPTR));
        actionRename->setText(QApplication::translate("ModbusMaster", "\351\207\215\345\221\275\345\220\215", Q_NULLPTR));
        actionStyle->setText(QApplication::translate("ModbusMaster", "Style", Q_NULLPTR));
        groupBox->setTitle(QString());
        groupBox_2->setTitle(QApplication::translate("ModbusMaster", "Config", Q_NULLPTR));
        rdbRTU->setText(QApplication::translate("ModbusMaster", "RTU", Q_NULLPTR));
        rdbASCII->setText(QApplication::translate("ModbusMaster", "ASCII", Q_NULLPTR));
        btnExpand->setText(QApplication::translate("ModbusMaster", "\346\250\241\346\213\237\350\256\276\345\244\207", Q_NULLPTR));
        btnPlus->setText(QApplication::translate("ModbusMaster", "+", Q_NULLPTR));
        btnSubtract->setText(QApplication::translate("ModbusMaster", "-", Q_NULLPTR));
        ckbInsertCRC->setText(QApplication::translate("ModbusMaster", "\350\207\252\345\212\250\346\267\273\345\212\240\346\240\241\351\252\214\347\240\201", Q_NULLPTR));
        lblTips->setText(QString());
        pushButton->setText(QApplication::translate("ModbusMaster", "\350\277\236\347\273\255\345\217\221\351\200\201", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        toolButton->setToolTip(QApplication::translate("ModbusMaster", "\346\270\205\347\251\272\346\226\207\346\234\254\346\241\206", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        toolButton->setText(QApplication::translate("ModbusMaster", "\346\270\205\347\251\272", Q_NULLPTR));
        QGBSerialConfig->setTitle(QApplication::translate("ModbusMaster", "\344\270\262\345\217\243\350\256\276\347\275\256", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        btnRefresh->setToolTip(QApplication::translate("ModbusMaster", "\345\210\267\346\226\260\347\253\257\345\217\243\345\217\267", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        btnRefresh->setText(QApplication::translate("ModbusMaster", "\345\210\267\346\226\260", Q_NULLPTR));
        btnOpenPort->setText(QApplication::translate("ModbusMaster", "\346\211\223\345\274\200\344\270\262\345\217\243", Q_NULLPTR));
        lblPortNum->setText(QApplication::translate("ModbusMaster", "\347\253\257\345\217\243\345\217\267", Q_NULLPTR));
        lblBaudRate->setText(QApplication::translate("ModbusMaster", "\346\263\242\347\211\271\347\216\207", Q_NULLPTR));
        lblDataBit->setText(QApplication::translate("ModbusMaster", "\346\225\260\346\215\256\344\275\215", Q_NULLPTR));
        lblVerify->setText(QApplication::translate("ModbusMaster", "\346\240\241\351\252\214", Q_NULLPTR));
        lblStopBit->setText(QApplication::translate("ModbusMaster", "\345\201\234\346\255\242\344\275\215", Q_NULLPTR));
        groupBox_8->setTitle(QApplication::translate("ModbusMaster", "\345\270\270\347\224\250\346\214\207\344\273\244", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        txt03SlaveAddr->setToolTip(QApplication::translate("ModbusMaster", "\344\273\216\346\234\272\345\234\260\345\235\200", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        txt03RegAddr->setToolTip(QApplication::translate("ModbusMaster", "\345\257\204\345\255\230\345\231\250\345\234\260\345\235\200", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        txt06SlaveAddr->setToolTip(QApplication::translate("ModbusMaster", "\344\273\216\346\234\272\345\234\260\345\235\200", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        btn03Send->setToolTip(QApplication::translate("ModbusMaster", "\350\257\273\345\217\226\345\244\232\344\270\252\345\257\204\345\255\230\345\231\250", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        btn03Send->setText(QApplication::translate("ModbusMaster", "03H", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        txt03RegNum->setToolTip(QApplication::translate("ModbusMaster", "\345\257\204\345\255\230\345\231\250\346\225\260\351\207\217", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        txt10RegNum->setToolTip(QApplication::translate("ModbusMaster", "\345\257\204\345\255\230\345\231\250\346\225\260\351\207\217", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        txt04SlaveAddr->setToolTip(QApplication::translate("ModbusMaster", "\344\273\216\346\234\272\345\234\260\345\235\200", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        lineEdit_10H_01->setToolTip(QApplication::translate("ModbusMaster", "1", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        txt06Value->setToolTip(QApplication::translate("ModbusMaster", "\345\257\204\345\255\230\345\231\250\345\200\274", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        txt06RegAddr->setToolTip(QApplication::translate("ModbusMaster", "\345\257\204\345\255\230\345\231\250", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        txt04RegAddr->setToolTip(QApplication::translate("ModbusMaster", "\345\257\204\345\255\230\345\231\250\345\234\260\345\235\200", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        lineEdit_10H_02->setToolTip(QApplication::translate("ModbusMaster", "2", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        txt10SlaveAddr->setToolTip(QApplication::translate("ModbusMaster", "\344\273\216\346\234\272\345\234\260\345\235\200", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        btn04Send->setToolTip(QApplication::translate("ModbusMaster", "\350\257\273\345\215\225\344\270\252\350\276\223\345\205\245\345\257\204\345\255\230\345\231\250", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        btn04Send->setText(QApplication::translate("ModbusMaster", "04H", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        txt10Value->setToolTip(QApplication::translate("ModbusMaster", "\345\215\201\345\205\255\350\277\233\345\210\266\347\232\204\346\225\260\346\215\256\345\200\274\357\274\214\345\217\257\344\273\245\347\224\250\347\251\272\346\240\274\351\232\224\345\274\200", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        lineEdit_10H_03->setToolTip(QApplication::translate("ModbusMaster", "3", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        btn10Send->setToolTip(QApplication::translate("ModbusMaster", "\345\206\231\345\244\232\344\270\252\345\257\204\345\255\230\345\231\250", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        btn10Send->setText(QApplication::translate("ModbusMaster", "10H", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        txt04RegNum->setToolTip(QApplication::translate("ModbusMaster", "\345\257\204\345\255\230\345\231\250\346\225\260\351\207\217", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        txt10RegAddr->setToolTip(QApplication::translate("ModbusMaster", "\345\257\204\345\255\230\345\231\250\345\234\260\345\235\200", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        btn06Send->setToolTip(QApplication::translate("ModbusMaster", "\345\206\231\345\215\225\344\270\252\345\257\204\345\255\230\345\231\250", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        btn06Send->setText(QApplication::translate("ModbusMaster", "06H", Q_NULLPTR));
        txt03Read->setPlaceholderText(QApplication::translate("ModbusMaster", "Read", Q_NULLPTR));
        txt04Read->setPlaceholderText(QApplication::translate("ModbusMaster", "Read", Q_NULLPTR));
        btnOtherSend->setText(QApplication::translate("ModbusMaster", "\345\205\266\344\273\226\346\214\207\344\273\244", Q_NULLPTR));
        label_5->setText(QApplication::translate("ModbusMaster", "4", Q_NULLPTR));
        label->setText(QApplication::translate("ModbusMaster", "\345\272\217\345\217\267", Q_NULLPTR));
        label_4->setText(QApplication::translate("ModbusMaster", "3", Q_NULLPTR));
        label_7->setText(QApplication::translate("ModbusMaster", "6", Q_NULLPTR));
        label_3->setText(QApplication::translate("ModbusMaster", "2", Q_NULLPTR));
        label_9->setText(QApplication::translate("ModbusMaster", "8", Q_NULLPTR));
        label_6->setText(QApplication::translate("ModbusMaster", "5", Q_NULLPTR));
        label_8->setText(QApplication::translate("ModbusMaster", "7", Q_NULLPTR));
        label_11->setText(QApplication::translate("ModbusMaster", "10", Q_NULLPTR));
        label_10->setText(QApplication::translate("ModbusMaster", "9", Q_NULLPTR));
        label_2->setText(QApplication::translate("ModbusMaster", "1", Q_NULLPTR));
        txtMessage->setHtml(QApplication::translate("ModbusMaster", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'SimSun'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" color:#8f8f8f;\">\347\241\254\347\237\263\347\224\265\345\255\220 \346\220\272\346\211\213\345\220\210\344\275\234,\345\205\261\345\210\233\346\234\252\346\235\245</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" color:#8f8f8f;\">http://www.ing10bbs.com/</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; color:#8f8f8f;\"><br /></p></body></html"
                        ">", Q_NULLPTR));
        toolBar->setWindowTitle(QApplication::translate("ModbusMaster", "toolBar", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class ModbusMaster: public Ui_ModbusMaster {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MODBUSMASTER_H
